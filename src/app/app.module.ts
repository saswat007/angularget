import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { InputformComponent } from './Mycomponent/inputform/inputform.component';
import { HeaderComponent } from './Mycomponent/header/header.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {ServiceService} from "./Mycomponent/service/service.service";
import { PostformComponent } from './Mycomponent/postform/postform.component';

@NgModule({
  declarations: [
    AppComponent,
    InputformComponent,
    HeaderComponent,
    PostformComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
