import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../Model/post';
import { Post2 } from '../Model/post2';

@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  static postdatas(postdata: Post2) {
    throw new Error('Method not implemented.');
  }
  // params2 = 2;
  // params3 = this.params2.toString();
  // params1 = new HttpParams().set('userId', this.params3);
  //https://cors-anywhere.herokuapp.com/
  constructor(private httpclient: HttpClient) {}
  _url =
    'https://thingproxy.freeboard.io/fetch/https://jsonplaceholder.typicode.com/comments';
  _url2 =
    'https://thingproxy.freeboard.io/fetch/https://jsonplaceholder.typicode.com/posts';

  getposts(value): Observable<Post[]> {
    console.log(value);
    let params1 = value;
    let params2 = params1.toString();
    let params3 = new HttpParams().set('postId', params2);
    return this.httpclient.get<Post[]>(this._url, { params: params3 });
  }

  postdatas(postdata): Observable<any> {
    return this.httpclient.post(this._url2, postdata);
  }
}
