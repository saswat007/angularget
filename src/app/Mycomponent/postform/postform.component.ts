import { Component, OnInit } from '@angular/core';
import { Post2 } from '../Model/post2';
import {ServiceService} from "../service/service.service";

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  postdata =new Post2(null,null,"","");
  postd : Post2;
  
  constructor(private _service : ServiceService) { }

  ngOnInit(): void {
  }

  onSubmit(){

    //console.log(this.postdata);
    this._service.postdatas(this.postdata).subscribe((data) => {
      this.postd = data;
      console.log("data sent successfully",this.postd);
    });
    


  }

  

}
