import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service/service.service';
import { Post } from '../Model/post';

@Component({
  selector: 'app-inputform',
  templateUrl: './inputform.component.html',
  styleUrls: ['./inputform.component.css'],
})
export class InputformComponent implements OnInit {
  constructor(private _service: ServiceService) {}
  post: Post[];

  ngOnInit(){
    // this._service.getposts().subscribe((data) => {
    //   this.post = data;
    //   console.log("data is from post",this.post);
    // });
  }
  onSubmit(value){
    //console.log(value);
    this._service.getposts(value).subscribe((data) => {
      this.post = data;
      console.log("data is from post",this.post);
      console.log(this.post.length)
    });
  }
}
